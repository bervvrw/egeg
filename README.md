# Fog Creek careers puzzle #

Solution of Fog Creek's puzzle for [Software Developer](https://www.fogcreek.com/jobs/Dev) 
position.


## Application Problem ##

Find a 9 letter string of characters that contains only letters from

`acdegilmnoprstuw`

such that the hash(the_string) is

`945924806726376`

if hash is defined by the following pseudo-code:

~~~~
  Int64 hash (String s) { 
      Int64 h = 7 
      String letters = "acdegilmnoprstuw"
      for(Int32 i = 0; i < s.length; i++) { 
          h = (h * 37 + letters.indexOf(s[i])) 
      } 
      return h 
  } 
~~~~

For example, if we were trying to find the 7 letter string where hash(the_string) 
was 680131[]()659347, the answer would be `leepadg`.


## Solution ##

Since given alphabet contains only 16 letters, we can convert any 9-letter word to
unique number between 0 and 687194[]()76735 (0xFFFFFFFFF). 
We can simply iterate by numbers calculating hash from each number until the answer is found.

It takes about an hour on average computer to find the answer. 
This solution can be easily optimized to use multiple threads. 

The answer is `promenade`. The are no other answers.